FROM registry.cern.ch/docker.io/library/centos:7

RUN yum -y install dnf
RUN dnf -y groupinstall "Development Tools" && dnf clean all
RUN dnf -y install epel-release centos-release-scl && dnf clean all
RUN dnf -y install \
    make \
    cmake3 \
    git \
    devtoolset-9 \
    bzip2-devel \
    && dnf clean all
RUN dnf -y install zeromq-devel boost-devel && dnf clean all
ENV PATH=/opt/rh/devtoolset-9/root/usr/bin:$PATH

# Python build dependencies CentOS
RUN dnf install -y gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel gdbm-devel ncurses-devel db4-devel wget && dnf clean all

RUN git clone --depth=1 https://github.com/pyenv/pyenv.git .pyenv
ENV PYENV_ROOT $HOME/.pyenv
ENV PATH $PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH
ENV eval "$(pyenv init -)"
ENV PYTHON_VERSION=3.9.6
RUN pyenv install ${PYTHON_VERSION}

ARG branch=v1.4.4
RUN git clone --depth 1 --branch ${branch} https://:@gitlab.cern.ch:8443/YARR/YARR.git

RUN pyenv global ${PYTHON_VERSION}

RUN cmake3 -S YARR -B build -DYARR_CONTROLLERS_TO_BUILD="Spec;NetioHW;Emu" -DENABLE_PYTHON=1
WORKDIR /build
RUN make -j$(nproc --ignore=2) && make install && rm -rf /build
WORKDIR /root
COPY ./pyconfig ./pyconfig
ENV PYTHONPATH="/YARR/lib:/YARR/python:${PYTHONPATH}"
