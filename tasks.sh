#!/bin/bash
#
# ITk demonstrator taskfile
#
# 2022, Gerhard Brandt <gbrandt@cern.ch>
#

alias run=./tasks.sh

export YARR_BRANCH=yarr_master

export GITLAB_PROJECTID=153039
export GITLAB_PROJECT=yarr-containers

export GITLAB_SERVER=gitlab.cern.ch
export GITLAB_ROOTGROUP=atlas-itk-pixel-systemtest
export GITLAB_SUBGROUP=itk-demo-sw/containers
# export GITLAB_FEATURE=feature/my-feature-0

export GITLAB_REGISTRY=gitlab-registry.cern.ch
export GITLAB_REGISTRY_IMAGE=${GITLAB_REGISTRY}/${GITLAB_ROOTGROUP}/${GITLAB_SUBGROUP}/${GITLAB_PROJECT}/${YARR_BRANCH}

echo "image: ${GITLAB_REGISTRY_IMAGE}"

# GITLAB_TOKEN=$(cat ./.gitlab-token)
# export GITLAB_TOKEN

export NAMESPACE="demi.${GITLAB_PROJECT}"
PORT=5009
PUID=$(id -u)
PGID=$(id -g)
PPWD=$(pwd)
export PUID PGID PPWD PORT

function all {
  # build
  # up
  help
}

function build {
  echo "Building Docker image"
  docker build -t ${GITLAB_REGISTRY_IMAGE}:latest -f ${YARR_BRANCH}.Dockerfile .
}

function rebuild {
  docker build -t ${GITLAB_REGISTRY_IMAGE}:latest . --no-cache
}

function push {
  docker login ${GITLAB_REGISTRY}
  docker push ${GITLAB_REGISTRY_IMAGE}:latest
}

function pull {
  docker login ${GITLAB_REGISTRY}
  docker pull ${GITLAB_REGISTRY_IMAGE}:latest
  # docker tag ${GITLAB_REGISTRY_IMAGE}:latest ${GITLAB_PROJECT}
}

function run {
  docker run --rm -it ${GITLAB_REGISTRY_IMAGE}:latest bash
  # docker run --rm -it ${GITLAB_PROJECT} bash
}

function exec {
  docker exec -it ${GITLAB_REGISTRY_IMAGE}:latest ${1-bash}
  docker exec -it ${GITLAB_PROJECT} ${1-bash}
}

function help {
  echo "usage: $0 <task> <args>"
  compgen -A function | cat -n
}

"${@:-all}"
