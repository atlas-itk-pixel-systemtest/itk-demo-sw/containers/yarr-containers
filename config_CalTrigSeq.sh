#!/usr/bin/env bash

flxconfigset() {
    echo "set $1 $2"
    value=$(($2))
    curl -sS -X 'GET' \
    "http://localhost:8000/flxconfigset/$1/$value" \
    -H 'accept: application/json' | jq
}

#e.g: ./config_CalTrigSeq.sh 3 2 #3=DIGSCANSEQB, 2 is device number

SCANTYPE=$1 #1=digitalA, 2=analogA, 3=digitalB                                                                                                                                                

DIGSCANSEQA=("817e" "6969" "6363" "a971" "a66a" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "566a" "566c" "5671" "5672" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969")
ANASCANSEQA=("817e" "6969" "6363" "a66a" "716a" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "566a" "566c" "5671" "5672" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6969" "6363" "a96a" "6a6a" "6969") #diff analog scan
DIGSCANSEQB=("817e" "817e" "aaaa" "63a6" "a66c" "936a" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "4e6a" "566c" "5671" "5672" "2e74" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa" "aaaa")



if [[ $SCANTYPE -eq 1 ]]
then
    echo "Digital Scan A"
    SEQ=("${DIGSCANSEQA[@]}")
elif [[ $SCANTYPE -eq 2 ]]
then
    echo "Analog Scan A"
    SEQ=("${ANASCANSEQA[@]}")
elif [[ $SCANTYPE -eq 3 ]]
then
    echo "Digital Scan B"
    SEQ=("${DIGSCANSEQB[@]}")
else
  echo "SCANTYPE=$SCANTYPE not supported"
  exit 1
fi

#write into FPGA memory                                                                                                                                                                                                                                                                   
echo "flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WE=0x1"
#                                                                                                                                                                                                                                                                                         
echo

for ((i = 0 ; i < 32 ; i++)); do                                                                                                                             addr=$(printf '%x\n' $i)
    # echo "flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WRADDR=0x${addr}"
    # echo "flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WRDATA=0x${SEQ[i]}"
    # flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WRADDR=0x${addr}
    # flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WRDATA=0x${SEQ[i]}
    flxconfigset YARR_FROMHOST_CALTRIGSEQ_WRADDR 0x${addr}
    flxconfigset YARR_FROMHOST_CALTRIGSEQ_WRDATA 0x${SEQ[i]}

done

echo
#turn of WE                                                                                                                                                                    
echo "flx-config -d $2 set YARR_FROMHOST_CALTRIGSEQ_WE=0x0"

